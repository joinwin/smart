package org.smart4j.sso.init;

import javax.servlet.ServletContext;

public interface ISmartInitializer {

    void init(ServletContext servletContext);
}
