package org.smart4j.framework.plugin;

public interface Plugin {

    void init();

    void destroy();
}
